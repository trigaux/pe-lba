# Running the code

To evaluate each model on the dvf data, use either

```python
python compare.py --no-normalize dvf
```

or 

```python
python compare.py dvf
```

depending on whether the data should be normalized.
More options are listed with

```python
python compare.py --help
```

To change the models to evaluate, edit the array `models` in `run` in
`compare.py`. For example:

```python
models = {
    'LinReg': LinReg(),
    'LinRegShift2': LinRegShift(shift=2), # to use only the last 2 datapoints for each entry
}
```

# Data

## Generation

To generate a data file, use the appropriate method of data.DataManager

```python
from data import DataManager

dm = DataManager()
dm.gen_dvf() # Generate the Etalab DVF dataset, by default from 2016 to 2021
```

# Evaluation

Each model should be evaluated on it's predictions for the last year of data,
using a normalized RMSE metric. There should be 12 test sets, the `i`-th asking
to predict the semester starting on month `i`, giving 12 RMSEs.

# Results

Normalized data:
![RMSEs on last year](images/results_normalized.png "RMSEs on last year")

Raw data:
![RMSEs on last year](images/results.png "RMSEs on last year")

The dashed lines corresponds to the mean of their algorithms

_These were generated using `python compare.py [--no-normalize] dvf_
