from sklearn.neural_network import MLPRegressor

from models.model import BlockModel, BlockShiftModel

class MLP(BlockModel):
    def __init__(self, layers=(20), n_epochs=200, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mlp = MLPRegressor(layers, max_iter=n_epochs)

    def fit(self, X, Y):
        self.mlp.fit(X, Y)

    def predict(self, X):
        return self.mlp.predict(X)

    def color_id(self):
        return 3, 0


class MLPShift(BlockShiftModel):
    def __init__(self, layers=(20), n_epochs=200, verbose=False, shift=2,
            *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mlp = MLPRegressor(layers, max_iter=n_epochs,
            random_state=self.rs, verbose=verbose)
        self.shift = shift

    def fit(self, X, Y):
        self.mlp.fit(X, Y)

    def predict(self, X):
        return self.mlp.predict(X)

    def color_id(self):
        return 3, 1





