from models.model import BlockModel

class DummyMed(BlockModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def fit(self, X, Y):
        self.median = Y.median()

    def predict(self, X):
        return [self.median for _ in range(len(X))]

    def color_id(self):
        return 0, 0





