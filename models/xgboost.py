from xgboost import XGBRegressor

from models.model import BlockModel, BlockShiftModel

class XGB(BlockModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.lr = XGBRegressor()

    def fit(self, X, Y):
        self.lr.fit(X, Y)

    def predict(self, X):
        return self.lr.predict(X)

    def color_id(self):
        return 2, 0


class XGBShift(BlockShiftModel):
    def __init__(self, shift=2, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.lr = XGBRegressor()
        self.shift = shift

    def fit(self, X, Y):
        self.lr.fit(X, Y)

    def predict(self, X):
        return self.lr.predict(X)

    def color_id(self):
        return 2, 1





