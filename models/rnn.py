import torch
from torch import nn

from models.model import RNNModel

class RNN(RNNModel):
    def __init__(self, input_size=1, output_size=1, hidden_dim=20, n_layers=1,
            eval_length=12, n_epochs=100, lr=0.01, batch_size=None,
            device=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.hidden_dim = hidden_dim
        self.n_layers = n_layers
        self.eval_length = eval_length
        self.n_epochs = n_epochs
        self.lr = lr
        self.batch_size = batch_size
        # TODO: pass on GPU if available and batch_size allows for it
        self.device = torch.device("cpu") if device is None else device

        self.do = nn.Dropout(p=0.1)
        self.rnn = nn.RNN(input_size, hidden_dim, n_layers, batch_first=True)
        # Fully connected layer
        self.fc = nn.Linear(hidden_dim, output_size)

    def forward(self, x, do=False):
        batch_size = x.size(0)

        hidden = self.init_hidden(batch_size)

        if do:
            out, hidden = self.rnn(self.do(x), hidden)
        else:
            out, hidden = self.rnn(x, hidden)
        out = self.fc(out[:,-self.eval_length:])

        return out, hidden

    def init_hidden(self, batch_size):
        hidden = torch.zeros(self.n_layers, batch_size, self.hidden_dim) \
                      .to(self.device)
        return hidden

    def color_id(self):
        return 4, 0





