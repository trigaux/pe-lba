from sklearn import linear_model

from models.model import BlockModel, BlockShiftModel

class LinReg(BlockModel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.lr = linear_model.LinearRegression()

    def fit(self, X, Y):
        self.lr.fit(X, Y)

    def predict(self, X):
        return self.lr.predict(X)

    def color_id(self):
        return 1, 0


class LinRegShift(BlockShiftModel):
    def __init__(self, shift=2, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.lr = linear_model.LinearRegression()
        self.shift = shift

    def fit(self, X, Y):
        self.lr.fit(X, Y)

    def predict(self, X):
        return self.lr.predict(X)

    def color_id(self):
        return 1, 1





