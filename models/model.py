import logging
import numpy as np
from sklearn.utils import gen_batches
import torch
from torch import nn
# from pytorch_ranger import Ranger
import matplotlib.pyplot as plt

class Model:
    def __init__(self, random_state=4242, *args, **kwargs):
        self.logger = logging.getLogger(__name__
                                      + '.'
                                      + self.__class__.__name__)
        self.logger.setLevel(logging.DEBUG)
        self.rs = random_state

    def prepareData(self, dm):
        raise NotImplementedError

    def fit(self, X, Y):
        raise NotImplementedError

    def predict(self, X):
        raise NotImplementedError

    def eval(self, metric, X, Y):
        raise NotImplementedError

    def color_id(self):
        raise NotImplementedError


class BlockModel(Model):
    def prepareData(self, dm):
        return dm.groupBy(months=6)

    def eval(self, metric, X, Y):
        return metric(self.predict(X), Y)

class BlockShiftModel(Model):
    def prepareData(self, dm):
        return dm.groupBy(months=6, nbFeatures=self.shift)

    def eval(self, metric, X, Y):
        return metric(self.predict(X), Y)

class RNNModel(Model, nn.Module):
    def __init__(self, one_normalization=False, normalize_month=True, *args,
            **kwargs):
        super().__init__(*args, **kwargs)
        super(Model, self).__init__()
        self.one_normalization = one_normalization
        self.normalize_month = normalize_month

    def prepareData(self, dm):
        return dm.rnn(self.one_normalization, self.normalize_month)

    def fit(self, X, Y):
        criterion = nn.MSELoss()
        optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
        ls = []
        for epoch in range(1, self.n_epochs + 1):
            if self.batch_size:
                perm = np.random.permutation(X.shape[0])
                batches = gen_batches(X.shape[0], self.batch_size)
                bl = []
                for batch in batches:
                    optimizer.zero_grad() # Clears existing gradients from previous epoch
                    output, hidden = self(X[perm[batch]])
                    loss = torch.sqrt(criterion(output, Y[perm[batch],-self.eval_length:]))
                    loss.backward()
                    bl.append(loss.item())
                    optimizer.step()
                ls.append(np.mean(bl))
            else:
                optimizer.zero_grad() # Clears existing gradients from previous epoch
                output, hidden = self(X)
                loss = torch.sqrt(criterion(output, Y[:,-self.eval_length:]))
                loss.backward()
                ls.append(loss.item())
                optimizer.step()

            if epoch % (self.n_epochs // 10) == 0:
                self.logger.info(f'Epoch: {epoch:4d}/{self.n_epochs}')
                self.logger.info(f'Loss: {loss.item():.4f}')
        return ls

    def predict(self, X):
        return self.forward(X, do=False)[0][:,-1,0].detach().numpy()

    def eval(self, metric, X, Y):
        return metric(self.predict(X), Y[:,-1,0].detach().numpy())





