from models.model import BlockModel

class Dummy(BlockModel):
    def __init__(self, time_period_length=1, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.time_period_length = time_period_length

    def fit(self, X, Y):
        pass

    def predict(self, X):
        return X.iloc[:,-12 // self.time_period_length]

    def color_id(self):
        return 0, 0





