import gzip
import hashlib
import logging
import math
import os.path
import pandas as pd
from scipy.sparse import csr_matrix
import shutil
from sklearn.preprocessing import StandardScaler, MaxAbsScaler
from timeit import default_timer as timer
import torch
import urllib.request

import matplotlib.pyplot as plt
import numpy as np

class DataManager:
    def __init__(self, data_dir='data', normalize=True, std=True):
        self.logger = logging.getLogger(__name__
                                      + '.'
                                      + self.__class__.__name__)
        self.logger.setLevel(logging.DEBUG)
        self.data_dir = data_dir
        self.normalize = normalize
        self.std = std

    def resetData(self):
        self.dt = None
        self.group_by = {}
        self.blocks = {}

    def gen_dvf(self, first_year=2016, last_year=2021,
            md5_hs='1b0c22f135ac7d1f9968621d38904cda'):
        data_filename = self.data_dir + f'/dvf-{first_year}-{last_year}.parquet'

        # Generate the data if it doesn't exists or is wrong
        if os.path.isfile(data_filename) and (md5_hs is None
                or hashlib.md5(open(data_filename,'rb').read())
                          .hexdigest() == md5_hs):
            return

        for i in range(first_year, last_year+1):
            if os.path.isfile(self.data_dir+'/dvf-'+str(i)+'.csv'):
                self.logger.info(f'skipping {i}')
                continue
            self.logger.info(f'downloading {i}')
            urllib.request.urlretrieve(
                'https://files.data.gouv.fr/geo-dvf/latest/csv/' + str(i)
                    + '/full.csv.gz',
                self.data_dir + '/dvf-' + str(i) + '.csv.gz'
            )
            self.logger.info(f'extracting {i}')
            with gzip.open(self.data_dir+'/dvf-'+str(i)+'.csv.gz', 'rb') as fi:
                with open(self.data_dir+'/dvf-'+str(i)+'.csv', 'wb') as fo:
                    shutil.copyfileobj(fi, fo)

        dt = pd.concat((pd.read_csv(
                self.data_dir + '/dvf-' + str(i) + '.csv',
                usecols=['date_mutation','valeur_fonciere','nature_mutation',
                         'adresse_nom_voie','code_postal','code_commune',
                         'nom_commune','code_departement',
                         'surface_reelle_bati','nombre_pieces_principales',
                         'surface_terrain','id_parcelle','longitude',
                         'latitude'],
                dtype={'code_commune': 'str', 'code_departement': 'str'},
                parse_dates=['date_mutation']
            ).query('nature_mutation == "Vente"')
             .assign(year=i)
             .drop('nature_mutation', axis=1)
            for i in range(first_year, last_year+1))
        )

        dt = dt[dt['surface_reelle_bati'].apply(
            lambda x: isinstance(x, float) and not math.isnan(x)
        )]
        dt['prix_m2'] = dt['valeur_fonciere'] / dt['surface_reelle_bati']
        dt = dt.assign(month=dt['date_mutation'].apply(
            lambda x: 12 * x.year + x.month - 1
        ))

        dt.to_parquet(self.data_dir+f'/dvf-{first_year}-{last_year}.parquet')

        return dt

    def load_dvf(self, first_year=2016, last_year=2021):
        data_filename = self.data_dir + f'/dvf-{first_year}-{last_year}.parquet'
        if not os.path.isfile(data_filename):
            self.logger.info(f'Generating the DVF data between {first_year} and {last_year}')
            dt = self.gen_dvf(first_year=first_year, last_year=last_year,
                md5_hs=None)
        else:
            start = timer()
            self.logger.info(f'Loading the DVF data between {first_year} and {last_year}')
            dt = pd.read_parquet(
                data_filename,
                columns=['code_commune','month','date_mutation','prix_m2'],
                engine='fastparquet',
            )
            self.logger.info(f'Loaded data in {timer()-start:.2f}s')

        self.resetData()
        # We ignore Grayan-et-l'Hôpital (33193) as it has aberrant data for
        # July 2017
        self.dt = dt.query(
            'code_commune != "33193" and prix_m2 < 20000'
        )

    def groupBy(self, months=6, nbFeatures=None):
        if (months, nbFeatures) not in self.group_by:
            # We only have 6 months of data in 2021, so we can't group by more
            # than 6 months otherwise the test data would be incomplete
            assert(1 <= months <= 12 and 12 % months == 0 and months <= 6)
            if months not in self.blocks:
                dt = self.dt.groupby(['code_commune', 'month']).size() \
                         .unstack(fill_value=0)
                first_month, last_month = dt.columns.min(), dt.columns.max()
                start = timer()
                self.blocks[months] = pd.DataFrame(data={
                    i: dt[range(i, i+months)].sum(axis=1)
                    for i in range(first_month, last_month-months+1)
                })
                if self.normalize:
                    if self.std:
                        scaler = StandardScaler()
                        scaler.fit(self.blocks[months].iloc[:,:-12].values.T)
                        self.blocks[months] = pd.DataFrame(
                            columns=self.blocks[months].columns,
                            data=scaler.transform(
                                self.blocks[months].values.T
                            ).T
                        )
                    else:
                        # MaxAbsScaler
                        self.blocks[months] = self.blocks[months].divide(
                                self.blocks[months].iloc[:,:-12]
                                    .max(axis=1).replace(0, 1),
                            axis=0
                        )
                        # X = self.blocks[months].iloc[:,-12:]
                        # plt.hist(X[X > 1.0].values.flatten(), bins=100)
                        # plt.show()
                        Y = self.blocks[months]
                        # print(Y[Y.max(axis=1) < 1.1])
                        # remove all samples with outlier test values
                        self.blocks[months] = Y[Y.max(axis=1) < 1.1]
                    # X = np.sort(self.blocks[months].values.flatten())
                    # plt.hist(X, bins=10)
                    # plt.show()
                self.logger.info(f'Grouped data into blocks of {months} months in {timer()-start:.2f}s')

            n = self.blocks[months].shape[1]
            if nbFeatures is None:
                assert(n % months == 0)
                train = (
                    self.blocks[months].iloc[:,range(0, n-12-months, months)]
                        .rename({
                            self.blocks[months].columns[i*months]: i
                            for i in range((n - 12) // months - 1)
                        }, axis=1),
                    self.blocks[months].iloc[:,n-12-months]
                )
                n = train[0].shape[1]
            else:
                train = (
                    pd.concat((
                        self.blocks[months]
                            .iloc[:,range(i, i+nbFeatures*months, months)]
                            .rename({
                                self.blocks[months].columns[i+j*months]: j
                                for j in range(nbFeatures)
                            }, axis=1)
                        for i in range(n-12-(nbFeatures+1)*months+1)
                    )),
                    pd.concat((
                        self.blocks[months]
                            .iloc[:,i+nbFeatures*months]
                        for i in range(n-12-(nbFeatures+1)*months+1)
                    ))
                )

            test_n = n if nbFeatures is None else nbFeatures
            test = [
                (
                    self.blocks[months]
                        .iloc[:,range(-i-months*test_n-1,-i-1,months)]
                        .rename({
                            self.blocks[months].columns[-i-1-(j+1)*months]:
                                test_n-j-1
                            for j in range(test_n)
                        }, axis=1),
                    self.blocks[months].iloc[:,-i-1]
                )
                for i in range(11, -1, -1)
            ]

            self.group_by[(months, nbFeatures)] = (train, test)

        return self.group_by[(months, nbFeatures)]

    def rnn(self, one_normalization=False, normalize_month=True):
        data_month = self.dt.groupby(['code_commune', 'month'])
        mon_cnt = data_month.size().reset_index().rename(columns={0: 'count'})
        mon_cnt['m'] = mon_cnt['month'] - mon_cnt['month'].min()
        mon_cnt['city'] = np.unique(mon_cnt['code_commune'], return_inverse=True)[1]
        rows = mon_cnt['m']
        cols = mon_cnt['city']
        M_data = mon_cnt['count']

        M = csr_matrix((M_data, (rows, cols)))
        sl = 6
        Y = np.array([M[j:j+sl].sum(axis=0).A1 for j in range(M.shape[0] - sl)])
        X = M[:-sl-1].todense()

        # normalize
        if self.normalize:
            Y_max = Y.max(axis=0)
            Y_max[Y_max == 0] = 1
            Y_scaled = Y / Y_max
            if normalize_month:
                if one_normalization:
                    X_max = X.max(axis=0)
                    X_max[X_max == 0] = 1
                    X_scaled = X / X_max
                else:
                    X_scaled = X / Y_max
            else:
                X_scaled = X
        else:
            X_scaled, Y_scaled = X, Y

        Y_scaled = Y_scaled[1:]

        # test_per = 0.25
        # full_test = 0
        # n = int(X.shape[0] * test_per)
        n = 12 + sl
        X_train, Y_train = torch.from_numpy(X_scaled[:-n].transpose()), torch.from_numpy(Y_scaled[:-n].transpose())
        X_test, Y_test = torch.from_numpy(X_scaled.transpose()), torch.from_numpy(Y_scaled.transpose())
        X_train, Y_train, X_test, Y_test = map(lambda x: x[:,:,None].to(torch.device("cpu")).float(), [X_train, Y_train, X_test, Y_test])
        train = X_train, Y_train
        test = [(X_test[:,:-11+i], Y_test[:,:-11+i]) for i in range(11)] \
            + [(X_test, Y_test)]
        return train, test





