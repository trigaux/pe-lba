import argparse
import logging
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import mean_squared_error as MSE
import sys
import time
import torch

from data import DataManager
from models import Dummy, DummyMed, LinReg, LinRegShift, MLP, MLPShift, RNN, \
        LSTM, XGB, XGBShift # noqa: F401

logger = logging.getLogger(__name__)

def run(dm: DataManager, verbose, plot_pe=False, normalize=True, n_epochs=100,
        dev=False):
    c = plt.rcParams['axes.prop_cycle'].by_key()['color']
    plt.rcParams.update({'font.size': 16})
    lw = [1.5, 3]
    if dev:
        models = {
            'MLP20Shift2': MLPShift(n_epochs=n_epochs, verbose=verbose),
            'RNN': RNN(n_epochs=n_epochs),
            'RNNone': RNN(n_epochs=n_epochs, one_normalization=True),
            'RNNmonth': RNN(n_epochs=n_epochs, normalize_month=False),
        }
    else:
        models = {
            # 'MLPShift1': MLPShift(shift=1),
            # 'MLPShift2': MLPShift(shift=2),
            # 'MLPShift3': MLPShift(shift=3),
            # 'MLPShift4': MLPShift(shift=4),
            # 'MLPShift5': MLPShift(shift=5),
            # 'MLPShift6': MLPShift(shift=6),
            'LinReg': LinReg(),
            'LinRegShift2': LinRegShift(),
            'XGB': XGB(),
            'XGBShift2': XGBShift(),
            'MLP20': MLP(n_epochs=n_epochs),
            'MLP20Shift2': MLPShift(n_epochs=n_epochs, verbose=verbose),
            # 'RNN20': RNN(n_epochs=n_epochs),
            # 'RNN15': RNN(n_epochs=n_epochs, hidden_dim=15),
            # 'LSTM15': LSTM(n_epochs=n_epochs, hidden_dim=15),
        }
    models['LastYear'] = Dummy(6)
    models['Median'] = DummyMed()

    taken = set()

    tests_X = [f'{x+1:02d}/2020' for x in range(12)]
    dpi = 96
    fs = (1920/dpi, 1080/dpi)
    if plot_pe:
        plt.figure(figsize=fs, dpi=dpi)
    else:
        fig, ax = plt.subplots(figsize=fs, dpi=dpi)

    def metric(x, y):
        return MSE(x, y, squared=False)
    y_test = []
    losses = {}
    for j, (m_name, model) in enumerate(models.items()):
        color, linewidth = model.color_id()
        while (color, linewidth) in taken and color < len(c)-1:
            color += 1
        taken.add((color, linewidth))
        color, linewidth = c[color], lw[linewidth]
        if m_name == "Median" and not normalize:
            continue
        if m_name == "LastYear" and normalize:
            continue
        train, test = model.prepareData(dm)
        ls = model.fit(*train)
        if ls is not None:
            losses[m_name] = ls
        tests_Y = []
        y_test.append([])
        if plot_pe:
            y_barre = []
            y = []
        for i, _ in enumerate(tests_X):
            y_test[-1].append(test[i][1])
            tests_Y.append(model.eval(metric, *test[i]))
            if plot_pe:
                y_barre.append(model.predict(test[i][0]))
                y.append(test[i][1])
        if plot_pe:
            y_barre = np.mean(y_barre, axis=0)
            y = np.mean(y, axis=0)
            id_sorted = np.argsort(y_barre)[::-1]
            n = y.shape[0]
            deciles = [
                np.sum(y[id_sorted[(n*i) // 10:(n*(i+1)) // 10]])
                for i in range(10)
            ]
            plt.bar(np.arange(10)-j*0.1, deciles, width=0.1, color=c[j],
                    label=m_name)
        else:
            ax.plot(tests_X, tests_Y, label=m_name, linewidth=linewidth,
                    c=color)
            ax.plot(tests_X, [np.mean(tests_Y)] * 12, '--',
                    linewidth=linewidth, c=color)
        print(f'{m_name}:', np.mean(tests_Y))
        print(model.eval(metric, *train))

    # check if the values for test are the same
    # for i in tests_X:
    #     print(metric(y_test[0][i], y_test[1][i][:,-1,0].detach().numpy()))

    t = time.strftime("%Y%m%d%H%M%S", time.localtime())
    pref = f'results/{t}-'
    if normalize:
        pref += 'norm-'
    if plot_pe:
        plt.legend()
        plt.savefig(f'{pref}pe.png', dpi=dpi)
        plt.clf()
    else:
        ax.set_title('RMSE by predicted semester')
        ax.set_xlabel('Start of semester')
        ax.set_ylabel('RMSE')
        ax.legend()
        plt.savefig(f'{pref}results.png', dpi=dpi)
        plt.clf()

    if len(losses) > 0:
        plt.figure(figsize=(1920/dpi, 1080/dpi), dpi=dpi)
        for m_name, ls in losses.items():
            plt.plot(ls, label=m_name)
        plt.legend()
        plt.savefig(f'{pref}loss.png', dpi=dpi)
        plt.clf()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Compare models')
    parser.add_argument('dataset',
                        type=str,
                        help='Dataset to use')
    parser.add_argument('--data-path',
                        dest='data-path',
                        type=str,
                        default='data',
                        help='Location of the data')
    parser.add_argument('--dataset-start',
                        dest='dataset-start',
                        type=int,
                        default=2016,
                        help='start of the dataset')
    parser.add_argument('--dataset-end',
                        dest='dataset-end',
                        type=int,
                        default=2021,
                        help='end of the dataset')
    parser.add_argument('--no-normalize',
                        dest='normalize',
                        action='store_false',
                        default=True,
                        help='Don\'t normalize the data by rows')
    parser.add_argument('-std', '--standard-normalize',
                        dest='std',
                        action='store_true',
                        default=False,
                        help='Use standard normalization')
    parser.add_argument('-peg', '--pe-graph',
                        dest='plot_pe',
                        action='store_true',
                        default=False,
                        help='Use PE performance graph')
    parser.add_argument('-it', '--n_epochs',
                        dest='n_epochs',
                        type=int,
                        default=100,
                        help='Number of epochs for iterative models')
    parser.add_argument('-d', '--dev',
                        dest='dev',
                        action='store_true',
                        default=False,
                        help='Dev mode')
    parser.add_argument('--verbose',
                        dest='verbose',
                        action='store_true',
                        default=False,
                        help='Turn on verbose for slow models')
    options = vars(parser.parse_args())

    dt_path = options.get('data-path')
    dataset = options.get('dataset')
    dt_start = options.get('dataset-start')
    dt_end = options.get('dataset-end')
    normalize = options.get('normalize')
    std = options.get('std')
    verbose = options.get('verbose')
    plot_pe = options.get('plot_pe')
    n_epochs = options.get('n_epochs')
    dev = options.get('dev')

    logging.basicConfig(format='%(asctime)s - [%(name)s] - '
                               '%(levelname)s: %(message)s')
    logger.setLevel(logging.DEBUG)

    dm = DataManager(dt_path, normalize, std)

    if dataset == 'dvf':
        dm.load_dvf(first_year=dt_start, last_year=dt_end)
    else:
        logger.error(f'The dataset {dataset} doesn\'t exists')
        sys.exit()

    torch.set_num_threads(5)
    run(dm=dm, verbose=verbose, plot_pe=plot_pe, normalize=normalize,
        n_epochs=n_epochs, dev=dev)





